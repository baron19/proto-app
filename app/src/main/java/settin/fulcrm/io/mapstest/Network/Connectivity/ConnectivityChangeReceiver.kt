package settin.fulcrm.io.mapstest.Network.Connectivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

/**
 * Created by nihar on 2/20/17.
 */
class ConnectivityChangeReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        NetworkManager.isConnected = connMgr.activeNetworkInfo != null
                && connMgr.activeNetworkInfo.isConnected
    }
}
