package settin.fulcrm.io.mapstest.ui.activities

import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.location.Location
import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import settin.fulcrm.io.mapstest.Location.*
import settin.fulcrm.io.mapstest.Network.Connectivity.NetworkManager
import settin.fulcrm.io.mapstest.Network.Data.*
import settin.fulcrm.io.mapstest.Network.ProtoService
import settin.fulcrm.io.mapstest.R
import settin.fulcrm.io.mapstest.data.Driver
import settin.fulcrm.io.mapstest.ui.Utils.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * Created by nihar on 2/17/17.
 */
class MapsActivity : BaseActivity(), OnMapReadyCallback {
    companion object {
        enum class Stage {
            PICKUP, DROPOFF, RIDE_REQUEST, RIDE_CONFIRMED, RIDE_IN_PROGRESS
        }

        val PLACE_AUTOCOMPLETE_REQ_CODE = 8000
    }

    private lateinit var mMap: GoogleMap

    private lateinit var mGeocoder: ReverseGeocoder
    private lateinit var mLocationProvider: LocationProvider

    // The current location of the device
    private lateinit var mDeviceLocation: Location

    // Address at the center of mMap at any moment the camera is idle
    private var mPointerLocation by Delegates.observable(LatLng(0.0, 0.0)) {
        prop, old, new ->
        run {
            // Get the address for this location
            mGeocoder.getAddressFromLatLng(new)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { mPointerAddress = it },
                            { mPointerAddress = mGeocoder.DEFAULT_ADDRESS })

            // Kick Off the request to get nearby drivers if we are in PICKUP
            // This is the entry point for drivers related stuff
            if (mCurrentStage == Stage.PICKUP) getNearByDrivers()
        }
    }
    private var mPointerAddress by Delegates.observable(Address(Locale.getDefault())) {
        prop, old, new ->
        setAddress()
    }

    // Camera State
    private var isCameraIdle by Delegates.observable(true) {
        prop, old, new ->
        if (!new) {
            setAddressLoading()
        }
    }

    // Keeping track of the stage we are in, also triggers stage change logic
    private var mCurrentStage by Delegates.observable(Stage.PICKUP) {
        prop, old, new ->
        run {
            setStage()
            setAddress()
        }
    }

    // Pick Up Location stuff, setting the mPickUpLocation automatically
    // triggers other events
    private var mPickUpLocation by Delegates.observable(LatLng(0.0, 0.0)) {
        prop, old, new ->
        run {
            // Get the address
            mGeocoder.getAddressFromLatLng(new)
                    .observeOn(Schedulers.io())
                    .subscribe(
                            { mPickUpAddress = it },
                            { /*Shouldn't come here*/ Timber.d("Error!: ${it.message}") })

            // Build the marker options
            mPickUpMarkerOptions = MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pickup))
                    .position(new)

            // Add marker to map
            mPickUpMarker = mMap.addMarker(mPickUpMarkerOptions)
        }
    }
    private lateinit var mPickUpAddress: Address
    private lateinit var mPickUpMarkerOptions: MarkerOptions
    private var mPickUpMarker: Marker? = null

    // Drop Off Location Stuff
    private var mDropOffLocation by Delegates.observable(LatLng(0.0, 0.0)) {
        prop, old, new ->
        run {
            // Get the address
            mGeocoder.getAddressFromLatLng(new)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { mDropOffAddress = it },
                            { /*Shouldn't come here*/ Timber.d("Error!: ${it.message}") })

            // Build the marker options
            mDropOffMarkerOptions = MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_dropoff))
                    .position(new)

            // Add marker to map
            mDropOffMarker = mMap.addMarker(mDropOffMarkerOptions)

            // Get and add the path to map
            PathProvider().getPolyLinePath(mPickUpLocation, new)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                mPath = mMap.addPolyline(
                                        PolylineOptions()
                                                .addAll(it)
                                                .width(10f)
                                                .color(Color.DKGRAY))

                                // Move the camera to show path
                                mMap.animateCamera(pathCameraUpdate(mPickUpLocation, new, it))
                            },
                            { /*Comes here after 3 retries*/ Timber.d("${it.message}") }
                    )
        }
    }
    private var mDropOffAddress: Address by Delegates.observable(Address(Locale.getDefault())) {
        prop, old, new ->
        run {
            mCurrentStage = Stage.RIDE_REQUEST
        }
    }
    private lateinit var mDropOffMarkerOptions: MarkerOptions
    private var mDropOffMarker: Marker? = null

    // The path from mPickUpLocation to mDropOffLocation
    private var mPath: Polyline? = null

    // NearBy Drivers related stuff
    private var mDrivers = ArrayList<Driver>()
    private val mDriverMarkers: HashMap<Driver, Marker> = HashMap()
    private val mDriverDisposables: CompositeDisposable = CompositeDisposable()

    // Ride Details
    private lateinit var mRideDetails: RideDetails

    /**
     * Views. Since we are loading different layouts for different actions
     * We need to keep track of them in code and not depend on kotlinx extensions.
     */
    private lateinit var mAddressTv: TextView
    private lateinit var mAddressLoadingBar: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        mLocationProvider = LocationProvider(this)
        mGeocoder = ReverseGeocoder(ctx)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        myLocationButton.setOnClickListener {
            doAction { mMap.animateCamera(myLocationCameraUpdate(mDeviceLocation.toAppLocation())) }
        }

        // Since network manager will be available during the entire duration of app
        // We need to unsubscribe to not leak this activity
        disposables.add(NetworkManager.connChangeNotif
                .subscribe {
                    if (!NetworkManager.isConnected) {
                        toast("No Internet, please check your connection")
                    }
                })

        // Set the initial state, Delegates.Observable doesn't call on initial value
        setStage()
    }

    override fun onStart() {
        super.onStart()
        mLocationProvider.start()
        mDrivers.forEach(Driver::startUpdates)
    }

    override fun onStop() {
        mDrivers.forEach(Driver::stopUpdates)
        mLocationProvider.stop()
        super.onStop()
    }

    override fun onDestroy() {
        cleanUpDrivers()
        super.onDestroy()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = false

        mMap.uiSettings.isCompassEnabled = false
        mMap.uiSettings.isRotateGesturesEnabled = false

        // Move camera to device location if we are at least 100m away
        mLocationProvider.getLocation()
                .subscribe {
                    var distance = Float.POSITIVE_INFINITY
                    try {
                        distance = mDeviceLocation.distanceTo(it)
                    } catch (e: UninitializedPropertyAccessException) {
                    } finally {
                        if (distance > 100) {
                            mDeviceLocation = it
                            mMap.moveCamera(deviceLocationCameraUpdate(mDeviceLocation.toAppLocation()))
                        }
                    }
                }

        mMap.setOnCameraMoveStartedListener {
            isCameraIdle = false
        }

        // When camera starts moving, we'll set the mPointerLocation
        mMap.setOnCameraIdleListener {
            isCameraIdle = true
            val mapCenter = mMap.cameraPosition.target
            mPointerLocation = LatLng(mapCenter.latitude, mapCenter.longitude)
        }
    }

    /**
     * Autocomplete callbacks
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQ_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(this, data)
                mMap.animateCamera(pointerLocationCameraUpdate(place.latLng))
            }
        }
    }

    /**
     * UI Updating functions
     */
    private fun setAddressLoading() {
        if (mCurrentStage == Stage.PICKUP || mCurrentStage == Stage.DROPOFF) {
            mAddressTv.visibility = View.INVISIBLE
            mAddressLoadingBar.visibility = View.VISIBLE
        }
    }

    private fun setAddress() {
        if (mCurrentStage == Stage.PICKUP || mCurrentStage == Stage.DROPOFF) {
            mAddressTv.visibility = View.VISIBLE
            mAddressLoadingBar.visibility = View.INVISIBLE

            mAddressTv.text = mPointerAddress.getShortAddress()
        }
    }

    private fun setStage() {
        when (mCurrentStage) {
            Stage.PICKUP -> {
                actionContainer.removeAllViews()
                layoutInflater.inflate(R.layout.action_pickup, actionContainer)

                mapCenterPointer.setImageResource(R.drawable.ic_map_pickup)
                mPickUpMarker?.remove()

                if (mPickUpLocation.distanceTo(LatLng(0.0, 0.0)) > 100) {
                    mMap.animateCamera(pointerLocationCameraUpdate(mPickUpLocation))
                }

                mAddressTv = find<TextView>(R.id.pickUpAddressTv)
                mAddressLoadingBar = find<LinearLayout>(R.id.pickUpAddressLoadingBar)

                // PlaceAutocomplete
                mAddressTv.setOnClickListener {
                    doAction {
                        val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .setBoundsBias(autocompleteBias(mPointerLocation))
                                .build(this)
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQ_CODE)
                    }
                }

                val confirmButton = find<Button>(R.id.confirmPickUpButton)
                confirmButton.setOnClickListener {
                    doAction {
                        if (mPointerAddress != mGeocoder.DEFAULT_ADDRESS) {
                            mPickUpLocation = mPointerLocation
                            mCurrentStage = Stage.DROPOFF
                        } else {
                            toast("Sorry, no valid address found")
                        }
                    }
                }
            }

            Stage.DROPOFF -> {
                actionContainer.removeAllViews()
                layoutInflater.inflate(R.layout.action_dropoff, actionContainer)

                mapCenterPointer.visibility = View.VISIBLE
                mapCenterPointer.setImageResource(R.drawable.ic_map_dropoff)

                // If we are coming back from RIDE_REQUEST
                mMap.uiSettings.isScrollGesturesEnabled = true
                mMap.uiSettings.isZoomGesturesEnabled = true
                myLocationButton.visibility = View.VISIBLE

                if (mDropOffLocation.distanceTo(LatLng(0.0, 0.0)) > 100) {
                    mMap.animateCamera(pointerLocationCameraUpdate(mDropOffLocation))
                }

                mDropOffMarker?.remove()
                mPath?.remove()

                mAddressTv = find<TextView>(R.id.dropOffAddressTv)
                mAddressLoadingBar = find<LinearLayout>(R.id.dropOffAddressLoadingBar)

                mAddressTv.setOnClickListener {
                    doAction {
                        val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .setBoundsBias(autocompleteBias(mPointerLocation))
                                .build(this)
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQ_CODE)
                    }
                }

                val confirmButton = find<Button>(R.id.confirmDropOffButton)
                confirmButton.setOnClickListener {
                    doAction {
                        if (mPointerAddress != mGeocoder.DEFAULT_ADDRESS) {
                            mDropOffLocation = mPointerLocation
                        } else {
                            toast("Sorry, no valid address found for this location")
                        }
                    }

                    // Don't do stage change here, since we need the dropoff address
                    // before we can move to RIDE_REQUEST. Instead change the
                    // Stage once we have the dropoff address.
                    // mCurrentStage = Stage.RIDE_REQUEST
                }
            }

            Stage.RIDE_REQUEST -> {
                actionContainer.removeAllViews()
                layoutInflater.inflate(R.layout.action_request_ride, actionContainer)
                mapCenterPointer.visibility = View.INVISIBLE

                mMap.uiSettings.isScrollGesturesEnabled = false
                mMap.uiSettings.isZoomGesturesEnabled = false

                myLocationButton.visibility = View.INVISIBLE

                val pickUpAddressTv = find<TextView>(R.id.pickUpAddressTv)
                pickUpAddressTv.text = mPickUpAddress.getShortAddress()

                val dropOffAddressTv = find<TextView>(R.id.dropOffAddressTv)
                dropOffAddressTv.text = mDropOffAddress.getShortAddress()

                // Fare estimate stuff
                val fareTv = find<TextView>(R.id.fareEstimateTv)
                val fareLoadingBar = find<ProgressBar>(R.id.fareEstimateLoadingBar)

                fareTv.visibility = View.INVISIBLE
                fareLoadingBar.visibility = View.VISIBLE

                ProtoService.getService()
                        .getFareEstimate(
                                FareEstimateReq.fromLatLng(mPickUpLocation, mDropOffLocation))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    fareTv.visibility = View.VISIBLE
                                    fareTv.text = it.fareEstimate.toString()
                                    fareLoadingBar.visibility = View.INVISIBLE
                                },
                                { Timber.d("${it.message}") })


                val paymentMethodBtn = find<Button>(R.id.paymentMethod)
                paymentMethodBtn.setOnClickListener {
                    /*TODO, start payment activity for result*/
                    toast("Sorry, that's a fake button :P")
                }

                val requestRideBtn = find<Button>(R.id.requestRide)
                requestRideBtn.setOnClickListener {
                    doAction {
                        actionContainer.removeAllViews()
                        layoutInflater.inflate(R.layout.action_loading, actionContainer)

                        ProtoService.getService()
                                .rideRequest(
                                        RideRequestReq.fromLatLng(mPickUpLocation, mDropOffLocation))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            mRideDetails = it.rideDetails
                                            mCurrentStage = Stage.RIDE_CONFIRMED
                                        },
                                        { Timber.d("${it.message}") }
                                )
                    }
                }
            }

            Stage.RIDE_CONFIRMED -> {
                actionContainer.removeAllViews()
                layoutInflater.inflate(R.layout.action_ride_confirmed, actionContainer)

                val driverNameTv = find<TextView>(R.id.driverName)
                driverNameTv.text = mRideDetails.driver.name

                /**
                 * TODO Here
                 *
                 * 1. Show ETA and location on map and path to mPickUpLocation for the driver.
                 *    Right now it just shows the driver kinda moving around randomly
                 *
                 * 2. Once the ride starts we need a 'service' which will be invoked by
                 *    a push notification from the server that the trip has started and
                 *    we move to the next stage
                 */

                // Since this a prototype just move to the next stage after let's say 10 seconds
                Single.just(true)
                        .delay(10, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { a -> mCurrentStage = Stage.RIDE_IN_PROGRESS }
            }

            Stage.RIDE_IN_PROGRESS -> {
                actionContainer.removeAllViews()

                /**
                 * TODO here
                 *
                 * As the trip starts we'll need to show the location
                 * as the user starts to move along the path. We already
                 * have the LocationProvider, so it should be easy to do it.
                 *
                 * For now just showing an empty frame on top
                 */
                rideInProgressContainer.visibility = View.VISIBLE
            }
        }
    }

    /**
     * Gets nearby drivers and puts in place code to animate them on mMap
     *
     * Gets called when the mPointerLocation changes
     */
    fun getNearByDrivers() {
        // First clean up earlier driver markers
        cleanUpDrivers()

        // Then get the new ones
        ProtoService.getService().nearByDrivers(NearByDriversReq.fromLatLng(mPointerLocation))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            mDrivers.addAll(it.drivers)
                            // Request updates on driver location and animate them on Map
                            mDrivers.forEach {
                                val driver = it
                                driver.init()
                                driver.deviceLocation = mPointerLocation.toAppLocation()
                                driver.startUpdates()
                                mDriverDisposables.add(driver.getDriverLocation()
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                                { animateDriver(driver, it.toLatLng()) },
                                                { Timber.d("${it.message}") }
                                        ))
                            }
                        },
                        { Timber.d("${it.message}") }
                )
    }

    fun animateDriver(driver: Driver, new: LatLng) {
        Timber.d("Animating Driver: $new")
        if (!mDriverMarkers.containsKey(driver)) {
            val marker = mMap.addMarker(MarkerOptions().position(new))
            mDriverMarkers.put(driver, marker)
        } else {
            val marker = mDriverMarkers[driver]!!
            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 1000
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener {
                val newLat = (new.latitude - marker.position.latitude) *
                        it.animatedFraction + marker.position.latitude

                val newLng = (new.longitude - marker.position.longitude) *
                        it.animatedFraction + marker.position.longitude

                marker.position = LatLng(newLat, newLng)
            }
            valueAnimator.start()
        }
    }

    fun cleanUpDrivers() {
        mDrivers.forEach(Driver::stopUpdates)
        for ((driver, marker) in mDriverMarkers) {
            marker.remove()
        }
        mDrivers.clear()
        mDriverDisposables.clear()
    }

    /**
     * Handles stage transitions
     */
    override fun onBackPressed() {
        when (mCurrentStage) {
            Stage.PICKUP -> super.onBackPressed()
            Stage.DROPOFF -> mCurrentStage = Stage.PICKUP
            Stage.RIDE_REQUEST -> mCurrentStage = Stage.DROPOFF
            Stage.RIDE_CONFIRMED -> super.onBackPressed()
            Stage.RIDE_IN_PROGRESS -> super.onBackPressed()
        }
    }
}
