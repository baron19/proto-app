package settin.fulcrm.io.mapstest.ui.Utils

import android.content.Context
import org.jetbrains.anko.toast
import settin.fulcrm.io.mapstest.Network.Connectivity.NetworkManager

/**
 * Created by nihar on 2/20/17.
 */
/**
 * An extension function which takes in a lambda, and runs it only if network is available.
 * Else shows a toast.
 *
 * For use with button clicks and other actions which are network dependant
 */
inline fun Context.doAction(code: () -> Unit) {
    if (NetworkManager.isConnected) {
        code()
    } else {
        toast("No internet, please check your connection")
    }
}
