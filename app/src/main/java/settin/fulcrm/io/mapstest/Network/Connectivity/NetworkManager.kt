package settin.fulcrm.io.mapstest.Network.Connectivity

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import io.reactivex.subjects.PublishSubject
import settin.fulcrm.io.mapstest.App
import kotlin.properties.Delegates

/**
 * Created by nihar on 2/20/17.
 */
object NetworkManager {
    var isConnected by Delegates.observable(true) {
        prop, old, new -> run {
            if (old != new) {
                connChangeNotif.onNext(true)
            }
        }
    }
    val connChangeNotif: PublishSubject<Any> = PublishSubject.create<Any>()

    private val receiver = ConnectivityChangeReceiver()

    init {
        // Set initial state of connectivity
        val connMgr = App.instance.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        isConnected = connMgr.activeNetworkInfo != null && connMgr.activeNetworkInfo.isConnected

        // Listen to changes in connectivity throughout duration of app
        val intentFiler = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        App.instance.registerReceiver(receiver, intentFiler)
    }
}
