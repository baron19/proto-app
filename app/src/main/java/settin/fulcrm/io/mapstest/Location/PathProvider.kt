package settin.fulcrm.io.mapstest.Location

import android.net.Uri
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonParser
import com.google.maps.android.PolyUtil
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.Request
import settin.fulcrm.io.mapstest.Network.HttpClient
import java.io.IOException
import java.net.URL

/**
 * Created by nihar on 2/18/17.
 */
class PathProvider {
    companion object {
        private val DIRECTIONS_API_KEY = "AIzaSyBrx5TkWTeCfnrnXXA25XimbaMDw_RmSSo"
        private val BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?"

        private val ORIGIN = "origin"
        private val DESTINATION = "destination"
        private val KEY = "key"
    }

    fun getPolyLinePath(pickUp: LatLng, dropOff: LatLng): Single<List<LatLng>>
            = Single.create<List<LatLng>>({
        try {
            val response = HttpClient.client.newCall(getRequest(pickUp, dropOff))
                    .execute()

            val respJson = response.body().string()
            val parser = JsonParser()
            val glob = parser.parse(respJson).asJsonObject

            // Should figure out a better way to do this, this is super fragile and ugly
            val polylineEncoded = glob.get("routes").asJsonArray[0]
                    .asJsonObject.get("overview_polyline").asJsonObject.get("points").asString

            it.onSuccess(PolyUtil.decode(polylineEncoded))
        } catch (e: IOException) {
            it.onError(e)
        }
    }).retry(3).subscribeOn(Schedulers.io())

    private fun getRequest(pickUp: LatLng, dropOff: LatLng): Request {
        val url = URL((Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter(ORIGIN, "${pickUp.latitude},${pickUp.longitude}")
                .appendQueryParameter(DESTINATION, "${dropOff.latitude},${dropOff.longitude}")
                .appendQueryParameter(KEY, DIRECTIONS_API_KEY)
                .build()).toString())

        val req = Request.Builder()
                .url(url)
                .get().build()

        return req
    }
}

