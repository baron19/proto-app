package settin.fulcrm.io.mapstest.Network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

/**
 * Created by nihar on 2/17/17.
 */
object HttpClient {
    val client: OkHttpClient by lazy {
        val log = HttpLoggingInterceptor { Timber.tag("Http").d(it)}
        log.level = HttpLoggingInterceptor.Level.BODY

        OkHttpClient.Builder()
                .addInterceptor(log)
                .build()
    }
}
