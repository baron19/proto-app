package settin.fulcrm.io.mapstest.Location

import android.content.Context
import android.location.Address
import android.location.Geocoder
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*

/**
 * Created by nihar on 2/18/17.
 */
class ReverseGeocoder(mContext: Context) {
    val DEFAULT_ADDRESS by lazy {
        val defaultAddress = Address(Locale.getDefault())
        defaultAddress.latitude = 0.0
        defaultAddress.longitude = 0.0
        defaultAddress.setAddressLine(0, "Address not found")
        defaultAddress.setAddressLine(1, "Please try another location")
        defaultAddress
    }

    private val geocoder: Geocoder = Geocoder(mContext, Locale.getDefault())

    fun getAddressFromLatLng(location: LatLng): Single<Address> = Single.create<Address>({
        try {
            val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)

            if (addresses == null || addresses.isEmpty()) {
                it.onError(IOException("No Address found"))
            } else {
                it.onSuccess(addresses[0])
            }
        } catch (e: IOException){
            it.onError(e)
        }
    }).retry(3).subscribeOn(Schedulers.io())
}
