package settin.fulcrm.io.mapstest.Location

import android.Manifest
import android.app.Activity
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import org.jetbrains.anko.ctx
import org.jetbrains.anko.toast
import timber.log.Timber

/**
 * Created by nihar on 2/18/17.
 *
 * Moved out from LocationProvider.
 *
 * Does three things.
 *
 * 1. Makes sure we have permission for ACCESS_FINE_LOCATION
 * 2. Makes sure GPS is turned on
 * 3. Runs mCode if both succeed, else finishes
 *
 * Cleans the whole thing up regardless of success or failure
 */
class LocationSettings(private val mActivity: Activity) :
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    companion object {
        val CONN_FAIL_RESOLUTION_REQ = 9000
        val PERMISSION_REQUEST_FINE_LOCATION = 9001
        val REQUEST_LOCATION_SETTINGS = 9002
    }

    private var permissionGranted = false
    private var apiClientConnected = false

    private lateinit var mCode: Activity.() -> Unit

    private val mGoogleApiClient = GoogleApiClient.Builder(mActivity.ctx)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

    private val mLocationRequest: LocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)
            .setFastestInterval(1 * 1000)

    private val mLocationSettingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
            .build()

    init {
        mGoogleApiClient.connect()
        checkPermission()
    }

    fun onPermissionsGranted(code: Activity.() -> Unit) {
        mCode = code
    }

    /**
     * Checks and requests runtime permission for ACCESS_FINE_LOCATION
     */
    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_REQUEST_FINE_LOCATION)
        } else {
            permissionGranted = true
            checkLocationSettings()
        }
    }

    fun onRequestLocationPermissionResult(grantResults: IntArray) {
        if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Timber.d("Permission granted")
            permissionGranted = true
            checkLocationSettings()
        } else {
            mActivity.toast("Sorry, need Location to work")
            cleanUp()
            mActivity.finish()
        }
    }

    /**
     * Handles Location settings
     */
    fun checkLocationSettings() {
        Timber.d("$permissionGranted | $apiClientConnected")
        if (permissionGranted && apiClientConnected) {
            LocationServices.SettingsApi.checkLocationSettings(
                    mGoogleApiClient,
                    mLocationSettingsRequest
            ).setResultCallback {
                when (it.status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                        mActivity.mCode()
                        cleanUp()
                    }

                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        it.status.startResolutionForResult(mActivity, REQUEST_LOCATION_SETTINGS)
                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        mActivity.toast("Couldn't turn on GPS")
                        cleanUp()
                        mActivity.finish()
                    }
                }
            }
        }
    }

    fun onLocationSettingsCheckResult(resultCode: Int) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                mActivity.mCode()
                cleanUp()
            }
            Activity.RESULT_CANCELED -> {
                mActivity.toast("Please turn on GPS")
                cleanUp()
                mActivity.finish()
            }
        }
    }

    fun cleanUp() {
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
    }

    /**
     * Connection callbacks from GoogleApiClient
     */
    override fun onConnected(connHints: Bundle?) {
        apiClientConnected = true
        Timber.d("Api Client Connected")
        checkLocationSettings()
    }

    override fun onConnectionSuspended(causeCode: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mActivity, CONN_FAIL_RESOLUTION_REQ)
            } catch (e: IntentSender.SendIntentException) {
                // Try again
                mGoogleApiClient.connect()
            }
        } else {
            Timber.d("Connection Failed with unrecoverable Error: %s", connectionResult.errorCode)
            cleanUp()
        }
    }
}