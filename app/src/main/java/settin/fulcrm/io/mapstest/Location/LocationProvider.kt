package settin.fulcrm.io.mapstest.Location

import android.app.Activity
import android.content.IntentSender
import android.location.Location
import android.os.Bundle
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.ctx
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by nihar on 2/17/17.
 */
class LocationProvider(private val mActivity: Activity) :
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    /**
     * https://github.com/googlesamples/android-play-location/issues/26
     *
     * FusedLocationApi has a problem with not releasing references to the LocationListener
     * we pass in, even after removeLocationUpdates. To stop the leak we use a WeakLocationListener
     * instead
     */

    companion object {
        val CONN_FAIL_RESOLUTION_REQ = 9000
    }

    private val location = PublishSubject.create<Location>()
    private val locationListener = LocationListener { location.onNext(it) }
    // We need this to avoid leaking the activity
    private val weakLocationListener = WeakLocationListener(WeakReference(locationListener))

    // TODO: Refactor out mGoogleApiClient from LocationSettings and LocationProvider
    private val mGoogleApiClient = GoogleApiClient.Builder(mActivity.ctx)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

    private val mLocationRequest: LocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)
            .setFastestInterval(1 * 1000)

    /**
     * Lifecycle methods to keep in sync with the enclosing activity
     */
    fun start() {
        mGoogleApiClient.connect()
    }

    fun stop() {
        if (mGoogleApiClient.isConnected) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, weakLocationListener)

            mGoogleApiClient.disconnect()
        }
    }

    fun getLocation(): Observable<Location> = location

    /**
     * Google api callbacks
     */
    override fun onConnected(connHints: Bundle?) {
        init()
    }

    override fun onConnectionSuspended(causeCode: Int) {
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mActivity, CONN_FAIL_RESOLUTION_REQ)
            } catch (e: IntentSender.SendIntentException) {
                // Try again
                mGoogleApiClient.connect()
            }
        } else {
            Timber.d("Connection Failed with unrecoverable Error: %s", connectionResult.errorCode)
        }
    }

    /**
     * Once the api client connects, We set up the location updates
     */
    private fun init() {
        val currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)

        if (currentLocation != null) {
            location.onNext(currentLocation)
        }

        LocationServices.FusedLocationApi
                .requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                        weakLocationListener)
    }
}
