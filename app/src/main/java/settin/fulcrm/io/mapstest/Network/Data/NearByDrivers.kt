package settin.fulcrm.io.mapstest.Network.Data

import com.google.android.gms.maps.model.LatLng
import settin.fulcrm.io.mapstest.data.Driver

/**
 * Created by nihar on 2/20/17.
 */
data class NearByDriversReq(val deviceLocation: Location) {
    companion object {
        fun fromLatLng(latLng: LatLng) = NearByDriversReq(Location(latLng.latitude, latLng.longitude))
    }
}

data class NearByDriversRsp(val drivers: List<Driver>)
