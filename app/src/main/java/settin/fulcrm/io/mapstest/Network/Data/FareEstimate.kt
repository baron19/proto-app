package settin.fulcrm.io.mapstest.Network.Data

import com.google.android.gms.maps.model.LatLng

/**
 * Created by nihar on 2/20/17.
 */
data class Location(val lat: Double, val lng: Double) {
    fun toLatLng(): LatLng = LatLng(lat, lng)
}

data class FareEstimateRsp(val fareEstimate: Double)
data class FareEstimateReq(val pickUpLocation: Location, val dropOffLocation: Location) {
    companion object {
        fun fromLatLng(pickUp: LatLng, dropOff: LatLng): FareEstimateReq {
            val pickUpLocation = Location(pickUp.latitude, pickUp.longitude)
            val dropOffLocation = Location(dropOff.latitude, dropOff.longitude)
            return FareEstimateReq(pickUpLocation, dropOffLocation)
        }
    }
}
