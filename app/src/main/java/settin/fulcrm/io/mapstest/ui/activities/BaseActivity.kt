package settin.fulcrm.io.mapstest.ui.activities

import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by nihar on 2/19/17.
 */
open class BaseActivity: AppCompatActivity() {
    // Don't really need to un-subscribe as all our Observables call onComplete sooner or later
    // The one that doesn't call onComplete is wrapped in a weakRef so it will get GCed anyway
    protected val disposables = CompositeDisposable()

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
}
