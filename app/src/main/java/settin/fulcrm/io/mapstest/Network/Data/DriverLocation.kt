package settin.fulcrm.io.mapstest.Network.Data

/**
 * Created by nihar on 2/20/17.
 */

// We need the deviceLocation here to fake the response from server side
data class DriverLocationReq(val id: Int, val deviceLocation: Location)

data class DriverLocationRsp(val id: Int, val driverLocation: Location)