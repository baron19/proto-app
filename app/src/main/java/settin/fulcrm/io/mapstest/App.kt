package settin.fulcrm.io.mapstest

import android.support.multidex.MultiDexApplication
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber
import timber.log.Timber.DebugTree

/**
 * Created by nihar on 2/17/17.
 */
class App: MultiDexApplication() {
    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)

        instance = this

        Timber.plant(DebugTree())
    }
}
