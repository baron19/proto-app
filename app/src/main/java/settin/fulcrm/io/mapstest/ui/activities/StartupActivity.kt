package settin.fulcrm.io.mapstest.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.startActivity
import settin.fulcrm.io.mapstest.Location.LocationSettings
import settin.fulcrm.io.mapstest.R

/**
 * Created by nihar on 2/17/17.
 */
class StartupActivity : AppCompatActivity() {
    private lateinit var mLocationSettings: LocationSettings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)

        mLocationSettings = LocationSettings(this)

        mLocationSettings.onPermissionsGranted {
            startActivity<MapsActivity>()
            finish()
        }
    }

    /**
     * Offloads the handling of request results to LocationSettings
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode) {
            LocationSettings.PERMISSION_REQUEST_FINE_LOCATION ->
                mLocationSettings.onRequestLocationPermissionResult(grantResults)

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    /**
     * Offloads the GPS turn on request to LocationSettings
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            LocationSettings.REQUEST_LOCATION_SETTINGS ->
                    mLocationSettings.onLocationSettingsCheckResult(resultCode)

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
