package settin.fulcrm.io.mapstest.ui.Utils

import android.location.Location as AndroidLocation
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.SphericalUtil
import settin.fulcrm.io.mapstest.Network.Data.Location as AppLocation

/**
 * Created by nihar on 2/20/17.
 */
fun AndroidLocation.toAppLocation(): AppLocation {
    return AppLocation(this.latitude, this.longitude)
}

fun myLocationCameraUpdate(target: AppLocation): CameraUpdate {
    val newPosition = CameraPosition.Builder()
            .target(LatLng(target.lat, target.lng))
            .zoom(18.0f)
            .build()

    return CameraUpdateFactory.newCameraPosition(newPosition)
}

fun deviceLocationCameraUpdate(target: AppLocation): CameraUpdate {
    val newPosition = CameraPosition.Builder()
            .target(LatLng(target.lat, target.lng))
            .zoom(16.0f)
            .build()

    return CameraUpdateFactory.newCameraPosition(newPosition)
}

fun pointerLocationCameraUpdate(target: LatLng): CameraUpdate {
    val newPosition = CameraPosition.Builder()
            .target(LatLng(target.latitude, target.longitude))
            .zoom(16.0f)
            .build()

    return CameraUpdateFactory.newCameraPosition(newPosition)
}

fun pathCameraUpdate(pickUp: LatLng, dropOff: LatLng, pathPoints: List<LatLng>): CameraUpdate {
    val boundsBuilder = LatLngBounds.builder().include(pickUp).include(dropOff)
    pathPoints.forEach {
        boundsBuilder.include(it)
    }
    val bounds = boundsBuilder.build()

    // This only works sometimes - weird, investigate further
    //return CameraUpdateFactory.newLatLngBounds(bounds, 0)

    // Basically computes zoom if we want the bounds to fit into 256 dp on screen
    val distance = SphericalUtil.computeDistanceBetween(bounds.northeast, bounds.southwest)/1000
    val zoom = 15 - Math.log(distance)/Math.log(2.0)
    return CameraUpdateFactory.newLatLngZoom(bounds.center, zoom.toFloat())
}
