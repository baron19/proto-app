package settin.fulcrm.io.mapstest.Location

import android.location.Address
import android.location.Location
import android.text.Html
import android.text.Spanned
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.SphericalUtil

/**
* Created by nihar on 2/17/17.
*/
fun Address.getShortAddress(): Spanned {
    val sB = StringBuilder()

    sB.append(this.getAddressLine(0))
    sB.append("<br/>")
    sB.append("<b>")
    sB.append(this.getAddressLine(1))
    sB.append("</b>")

    return Html.fromHtml(sB.toString())
}

fun LatLng.distanceTo(destination: LatLng) =
        SphericalUtil.computeDistanceBetween(this, destination)

fun LatLng.toAppLocation() =
        settin.fulcrm.io.mapstest.Network.Data.Location(this.latitude, this.longitude)

// Should probably make this an ext on the IntentBuilder which internally calls setLatLngBounds
fun autocompleteBias(center: LatLng): LatLngBounds =
        LatLngBounds.builder()
                .include(LatLng(center.latitude + 1.0, center.longitude + 1.0)) // Approx northEast
                .include(LatLng(center.latitude - 1.0, center.longitude - 1.0)) // Approx Southwest
                .build()
