package settin.fulcrm.io.mapstest.Network

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by nihar on 2/20/17.
 */
object ProtoService {
    private val BASE_URL = "http://104.155.128.174"
    private var service: ProtoAPI? = null

    fun getService(): ProtoAPI {
        if (service != null) {
            return service!!
        } else {
            val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create()

            val builder = Retrofit.Builder()
                    .client(HttpClient.client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(BASE_URL)

            service = builder.build().create(ProtoAPI::class.java)

            return service!!
        }
    }
}
