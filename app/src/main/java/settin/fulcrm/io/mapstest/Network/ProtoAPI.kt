package settin.fulcrm.io.mapstest.Network

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap
import settin.fulcrm.io.mapstest.Network.Data.*

/**
 * Created by nihar on 2/20/17.
 */
interface ProtoAPI {
    @POST("/fare_estimate")
    fun getFareEstimate(@Body req: FareEstimateReq): Observable<FareEstimateRsp>

    @POST("/ride_request")
    fun rideRequest(@Body req: RideRequestReq): Observable<RideRequestRsp>

    @POST("/nearby_drivers")
    fun nearByDrivers(@Body req: NearByDriversReq): Observable<NearByDriversRsp>

    // Used within driver data class, don't call directly
    @POST("/driver_location")
    fun driverLocation(@Body req: DriverLocationReq): Observable<DriverLocationRsp>
}