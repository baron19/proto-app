package settin.fulcrm.io.mapstest.data

import com.google.gson.annotations.Expose
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import settin.fulcrm.io.mapstest.Network.Data.DriverLocationReq
import settin.fulcrm.io.mapstest.Network.Data.Location
import settin.fulcrm.io.mapstest.Network.ProtoService
import java.util.concurrent.TimeUnit

/**
 * Created by nihar on 2/19/17.
 */
class Driver(val name: String, val id: Int) {
    // This has to be lateinit since Gson creates this class from parsing HttpResponse
    // and this field is still being set to initialized properly
    // TODO: Investigate further
    @Expose(serialize = false, deserialize = false)
    private lateinit var driverLocation: PublishSubject<Location>
    fun init() {
        driverLocation = PublishSubject.create()
    }

    private lateinit var disposable: Disposable

    fun getDriverLocation(): Observable<Location> = driverLocation

    // We need device location so that server can fake the driver being near device
    var deviceLocation: Location = Location(0.0, 0.0)

    // This is not good, polling for status every five seconds
    // Try to see if this can be done through websockets, since it can push continuously
    // and also has smaller network overhead compared to HTTP on subsequent requests
    fun startUpdates() {
        disposable = ProtoService.getService()
                .driverLocation(DriverLocationReq(id, deviceLocation))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .retry()
                .repeatWhen{ it.delay(5, TimeUnit.SECONDS)}
                .subscribe {
                    driverLocation.onNext(it.driverLocation)
                }
    }

    fun stopUpdates() {
        if (!disposable.isDisposed) disposable.dispose()
    }
}
