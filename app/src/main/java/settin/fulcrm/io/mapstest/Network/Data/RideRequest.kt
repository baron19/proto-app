package settin.fulcrm.io.mapstest.Network.Data

import com.google.android.gms.maps.model.LatLng
import settin.fulcrm.io.mapstest.data.Driver

/**
 * Created by nihar on 2/20/17.
 */

data class RideRequestReq(val pickUpLocation: Location, val dropOffLocation: Location) {
    companion object {
        fun fromLatLng(pickUp: LatLng, dropOff: LatLng): RideRequestReq {
            val pickUpLocation = Location(pickUp.latitude, pickUp.longitude)
            val dropOffLocation = Location(dropOff.latitude, dropOff.longitude)
            return RideRequestReq(pickUpLocation, dropOffLocation)
        }
    }
}

data class RideDetails(val driver: Driver)
data class RideRequestRsp(val rideDetails: RideDetails)