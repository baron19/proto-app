package settin.fulcrm.io.mapstest.Location

import android.location.Location
import com.google.android.gms.location.LocationListener
import java.lang.ref.WeakReference

/**
 * Created by nihar on 2/17/17.
 */
class WeakLocationListener(val locationListenerRef: WeakReference<LocationListener>):
        LocationListener{
    override fun onLocationChanged(location: Location)  =
        if (locationListenerRef.get() != null)
            locationListenerRef.get().onLocationChanged(location)
        else {}
}