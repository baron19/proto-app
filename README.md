<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org151f188">1. Introduction</a></li>
<li><a href="#orga4c83e2">2. App Prototype</a>
<ul>
<li><a href="#org60d7692">2.1. Location</a>
<ul>
<li><a href="#org96e5080">2.1.1. LocationSettings</a></li>
<li><a href="#org44eaff6">2.1.2. LocationProvider</a></li>
<li><a href="#orgad4e2c2">2.1.3. PathProvider</a></li>
<li><a href="#org6ecf42f">2.1.4. ReverseGeoCoder</a></li>
</ul>
</li>
<li><a href="#org63f582d">2.2. Network</a>
<ul>
<li><a href="#orgf230d2a">2.2.1. ProtoAPI</a></li>
<li><a href="#orgf84f27a">2.2.2. ProtoService</a></li>
<li><a href="#org52bd613">2.2.3. HttpClient</a></li>
<li><a href="#org98ac4e7">2.2.4. Data</a></li>
<li><a href="#orgb6be081">2.2.5. Connectivity</a></li>
</ul>
</li>
<li><a href="#org573477a">2.3. Data</a>
<ul>
<li><a href="#orge745f5f">2.3.1. Driver</a></li>
</ul>
</li>
<li><a href="#orgce26e33">2.4. Activities</a>
<ul>
<li><a href="#orgea7e535">2.4.1. BaseActivity</a></li>
<li><a href="#org6b5fa6e">2.4.2. Startup</a></li>
</ul>
</li>
<li><a href="#org3a27304">2.5. MapsActivity</a>
<ul>
<li><a href="#org6f0fc1f">2.5.1. Core</a></li>
<li><a href="#orgf923b6d">2.5.2. mPointerLocation</a></li>
<li><a href="#org7adffdd">2.5.3. mDrivers/getNearByDrivers()</a></li>
<li><a href="#org682912a">2.5.4. mPickUpLocation</a></li>
<li><a href="#org93688bc">2.5.5. mDropOffLocation</a></li>
<li><a href="#org0f254fa">2.5.6. Stage Transitions</a></li>
<li><a href="#org275413c">2.5.7. PlacePicker</a></li>
<li><a href="#org09938b5">2.5.8. Flow</a></li>
</ul>
</li>
<li><a href="#org3866afe">2.6. Comments</a></li>
<li><a href="#orgd398b9a">2.7. What's left out and improvements.</a></li>
</ul>
</li>
<li><a href="#orgedbf866">3. Backend</a></li>
</ul>
</div>
</div>


<a id="org151f188"></a>

# Introduction

I made a small prototype of your user facing app in the hopes that it might help my application process and show some of my skills. 
What follows are the details of the implementation.


<a id="orga4c83e2"></a>

# App Prototype

-   Repo: <https://gitlab.com/baron19/proto-app>

-   Written in Kotlin. My kotlin is only about 3 weeks old and while I tried to use some of the nice features of the language, 
    I'm still leaning the various patterns to organize code and it might not be idiomatic.

-   Libraries used apart from google SDK and play-services libraries.
    -   RxJava2 - Glue linking various asynchronous parts.
    
    -   Retrofit2, OkHttp3, Gson, Retrofit2 RxJava2 adapter - Network related parts.
    
    -   Anko - Kotlin library which makes implementing some common patterns in android simple.
    
    -   Kotlin-android-extensions - Similar to anko, for working with views, kind of replaces butterknife with no runtime overhead.
    
    -   LeakCanary - To detect Leaks.

-   I implemented only the core parts, choosing pickup and dropoff and requesting rides.

-   Uses a mock Backend described below, hosted on Google Compute.


<a id="org60d7692"></a>

## Location


<a id="org96e5080"></a>

### LocationSettings

-   Handles permissions and makes sure suitable location settings are turned on. Used in StartupActivity.
-   Takes in some code to be run in activity's context and runs it once all the permissions are available.
-   Cleans itself up at the end regardless of failure or success so that we don't need to manage the 
    resources to avoid leaks.


<a id="org44eaff6"></a>

### LocationProvider

-   Attaches itself to the activity and provides continuous location updates through an Observable<Location>.
-   Starts and stops in sync with activity onStart and onStop.
-   Cleans itself up in onDestroy()


<a id="orgad4e2c2"></a>

### PathProvider

-   Takes in two locations and provides a path in between them using google-directions API.
-   Returns a Single which runs off the mainThread and retries 3 times at the most.


<a id="org6ecf42f"></a>

### ReverseGeoCoder

-   Takes in a Location and returns the address using built-in Geocoder.
-   Returns a Single which runs off the mainThread and retries 3 times at the most.


<a id="org63f582d"></a>

## Network


<a id="orgf230d2a"></a>

### ProtoAPI

-   Retrofit2 API declaration. All requests return Observables.

-   Provides 4 EndPoints to the mock Backend
    -   Fare Estimate - Given pickUp and dropOff provides an estimated fare.
    
    -   Near by drivers - Given the device location, provides a list of nearby drivers.
    
    -   Driver Location - Given a driver ID, returns the driver's latest location. (Also takes in device location, so the server can 
        fake drivers being near by). Never called directly, instead called from Driver data class.
    
    -   Ride Request - Given pickUp and dropOff, confirms the ride and returns the ride details.


<a id="orgf84f27a"></a>

### ProtoService

-   Singleton Kotlin 'object' which lazily builds the retrofit service.


<a id="org52bd613"></a>

### HttpClient

-   OkHttp3 client with logging interceptor, just to be able to log requests and responses.


<a id="org98ac4e7"></a>

### Data

-   POJOs for Request and Response types for the different Endpoints, with some utility methods to build them.


<a id="orgb6be081"></a>

### Connectivity

-   A broadcast receiver which gets notified of connectivity changes through-out the app lifetime.

-   NetworkManager: A single kotlin 'object' which maintains the current network state and notifies of changes in state
    via an Observable. Also registers and un-registers the broadcast receiver.


<a id="org573477a"></a>

## Data

-   Has only one class. Since the User part is left out, there isn't a lot of data that needs to be kept in memory.


<a id="orge745f5f"></a>

### Driver

-   Data class for Driver, which is created by Gson from Http response.

-   Provides the location updates for this driver through an Observable.

-   Provides methods to start and stop updates.

-   When startUpdates() is called it polls the server for this driver's location using retrofit every 5 seconds and publishes
    that information through the above observable.

-   Cleans itself up when discarded.


<a id="orgce26e33"></a>

## Activities


<a id="orgea7e535"></a>

### BaseActivity

-   Base for all other activities. Has a composite disposable to keep rx subscriptions and discards them in onDestroy.


<a id="org6b5fa6e"></a>

### Startup

-   Checks and requests permissions using a LocationSettings object and makes sure GPS is turned on. Once we have the required
    permissions it just launches MapsActivity.


<a id="org3a27304"></a>

## MapsActivity

These are the main pieces in the code, everything else revolves around them.


<a id="org6f0fc1f"></a>

### Core

-   Business logic is implemented as a state machine with 5 states. PICKUP, DROPOFF, RIDE\_REQUEST, RIDE\_CONFIRMED, RIDE\_IN\_PROGRESS which 
    are declared explicitly in the enum 'Stage' as a companion object to the activity. The transitions exist only between adjacent states.

-   Current stage is tracked in mCurrentStage. This is implemented with Delegates.Observable so that stage transition logic 
    is kicked off when the stage changes.

-   Initial stage when starting up is PICKUP, which is set in onCreate.

-   All the stage change logic is implemented in setStage()

-   Each stage loads a new 'action' layout at the bottom of the activity root view, which has the appropriate Views
    for that stage.

-   Each Stage also cleans up things from previous stage if there are any, like markers.


<a id="orgf923b6d"></a>

### mPointerLocation

The Location under the pointer at the center of the map at any moment the camera is idle.

-   Is set inside onCameraIdleListener callback of the google map object.

-   Implemented with kotlin's Delegates.Observable, so when mPointerLocation is set geocoder is kicked into action to set the address
    in mPointerAddress

-   Once the mPointerAddress is set, it kicks off rendering the TextView at the bottom in the 'action' layout. Happens only in 
    PICKUP and DROPOFF stages, where we need to show the address.

-   Kicks off getting the nearby drivers data from backend.


<a id="org7adffdd"></a>

### mDrivers/getNearByDrivers()

The list of nearby drivers, requested when mPointerLocation changes.

-   Makes a network request to get the drivers.

-   Sets up updates to the driver locations through the Driver data class described above.

-   Declares code to animate the drivers when the result arrives.

-   Starts/stops the driver location in sync with activity onStart/onStop

-   Clean up happens in onDestroy()


<a id="org682912a"></a>

### mPickUpLocation

The location where the user clicked Confirm Pickup button

-   Gets set when the 'Confirm Pickup' button is clicked when the stage is PICKUP

-   This also causes the stage to move into DROPOFF.

-   Implemented via Delegates.Observable and setting this kicks off the following code
    -   Geocoder to get the address and set mPickUpAddress.
    
    -   Builds MarkerOptions with mPickUpLocation as target.
    
    -   Adds the marker to the map and assigns it to mPickUpMarker in case we need to remove it later.


<a id="org93688bc"></a>

### mDropOffLocation

The Location where the user clicked Confirm Dropoff button. This is very similar to mPickUpLocation above.

-   Gets set when Confirm DropOff is clicked and the stage is DROPOFF

-   This causes the stage to move to RIDE\_REQUEST.

-   Implemented via Delegates.Observable and setting this kicks off the following code
    -   Geocoder to get the dropoff address and set mDropOffAddress
    
    -   Builds marker options and adds the marker to the map.
    
    -   Kicks off 'PathProvider' to get the path between mPickUpLocation and mDropOffLocation. Also declares 
        code to move camera to show the path when the request completes.


<a id="org0f254fa"></a>

### Stage Transitions

Handled in onBackPressed, since setting the stage kicks off the transition logic, all we have to do is set 'mCurrentStage'.

-   PICKUP -> super.onBackPressed
-   DROPOFF -> PICKUP
-   RIDE\_REQUEST -> DROPOFF
-   RIDE\_CONFIRMED -> super.onBackPressed
-   RIDE\_IN\_PROGRESS -> super.onBackPressed


<a id="org275413c"></a>

### PlacePicker

Google's play-services-places provides the placepicker, nothing customized, triggered when the address bar is clicked 
when the stage is in PICKUP or DROPOFF.


<a id="org09938b5"></a>

### Flow

The above are the different pieces. This is how they are linked together and the sequence of the actions.

1.  When user moves the camera, the logic mentioned above in mPointerLocation is run.

2.  User picks a location either by moving the camera or using the placepicker and clicks 'confirm'. This will kick off 
    all the logic described in 'mPickUpLocation' section and then moves the stage to DROPOFF

3.  User again picks a dropoff location and clicks 'confirm'. This will kick off the logic described in 'mDropOffLocation'
    and then moves the stage to RIDE\_REQUEST.

4.  User clicks 'Request Ride' which kicks off a HTTP request to get the ride details and moves onto RIDE\_CONFIRMED
    stage once it get a response.

5.  User waits for the ride to arrive, but this is not implemented. Currently it just moves to the next Stage after a delay


<a id="org3866afe"></a>

## Comments

-   Orientation changes are not an issue since everything that's not related to activity lifecycle is separated out,
    although the UI should be customized a bit to move the 'action' layout to the right side instead of at the bottom 
    when in landscape mode.

-   No context leaks due to Rx, since most of them are either 'Single's, or Observables that call onComplete.
    The exceptions being NetworkManager and Driver location updates, they are disposed in the proper lifecycle callbacks.

-   FusedLocationApi leaks context, so I had to use a WeakReference there to avoid that. It's a well documented issue
    with play-services-location library.


<a id="orgd398b9a"></a>

## What's left out and improvements.

-   No user accounts or related data like past rides and payments. I felt it makes the data model much simpler yet
    decent enough for prototyping purposes.

-   No unit tests/instrumentation tests or UI tests for the code.

-   There's no architectural pattern implemented. I felt that I shouldn't over-engineer before there are 
    any good reasons to do so. Because of this the MapsActivity maybe a little bloated with all the variables and 
    logic inside.

-   Didn't get to optimize the build, so it's a bit large for such small amount of code. Kotlin runtime and stdlib 
    add about 1.5 MB to the size.

-   Didn't try to spend much time on UI, just used defaults from Material theme everywhere. It can be improved a lot,
    especially using anko.
    
    Anko's main feature is a DSL for declaring layouts in code. It has a neat way of separating out layouts with something 
    they call 'AnkoComponents'. I haven't yet tested it but the library claims orders of magnitude speed up in creating the layouts, 
    since there are no xml files to parse. I haven't yet familiarized myself properly with it, so I didn't use it in this case.

-   There's no persistence for the data in MapsActivity, if android decides to kill and recreate the activity, everything resets.
    This can be done in the usual way, but I didn't want to just put everything in a bundle, since there are quite a few values here.
    
    I was thinking I could separate out the various values into a data class and persist it in onStop and retrieve  
    in onStart. This will be easier for us to manage, and also less dependent on the android framework. This leaves 
    only rendering of the views in the activity code.

-   I should've used fragments instead of 'action' layouts at the bottom, would've made the activity cleaner and all the logic for a stage
    would be neatly organized inside the related fragment. This should be done after the above step with data persistence is implemented.

-   It's not robust in handling errors and other corner cases. The RxJava2 errors are handled by just retying for all errors, which 
    can be improved upon using info about what caused the error.

-   There's no backend to simulate the driver arriving with an ETA or driver starting the trip for RIDE\_CONFIRMED 
    or RIDE\_IN\_PROGRESS stages.

-   The GoogleApiClient has a lot more corner cases than I handled, it could be made much more robust.


<a id="orgedbf866"></a>

# Backend

-   Very simple mock API, written in Rust. Repo -> <https://gitlab.com/baron19/proto-backend>

-   Running on Google compute. Access Point -> <http://104.155.128.174/>

-   It's not secure, has no versioning, uses a HashMap as the driver Db.

-   All requests are implemented as POST since it can carry the parameters as JSON body. I know it doesn't sound RESTful, but since 
    the requests are independent with respect to the parameters, GET in this case will not provide any advantage over POST.

-   Has the following EndPoints 
    -   Near By Drivers - Takes in the device Location, and gives two drivers with random locations within 500 meters.
    
    -   Driver Location - Takes in the device location and moves the driver by about 10 meters in a random direction and returns 
        that as a new location.
    
    -   Ride Request - Takes in pickup and dropoff and returns a driver in a random nearby location.
    
    -   Fare Estimate - Gets the PickUp and DropOff Locations and provides a fare estimate for the ride, currently just computes
        the distance between them and returns a multiple of that.

-   Anything else returns a 404

